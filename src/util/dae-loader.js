/**
 * Created by zhoumingrui on 2017/1/12.
 */

function loadDae(constoller, imageManager, vertexManager) {
    var xml = require('../assets/dae/CM_CN_BEIJING_2477029672_H.dae');
    prepareData(constoller, xml, imageManager, vertexManager);
    return xml;
}

function prepareData(controller, xml, imageManager, vertexManager) {
    let gl = controller.gl;
    /**
     * 第一步准备 顶点数据 和 索引数据
     * @type {Float32Array}
     */
    /*
     * 获取模型url
     */
    let collada = xml.COLLADA;
    let library_visual_scenes = collada.library_visual_scenes;
    let visual_scene = library_visual_scenes[0].visual_scene;
    let node = visual_scene[0].node;
    let instance_geometry = node[0].instance_geometry;
    let url = instance_geometry[0].$.url;

    /**
     * 调用imageManager开始管理图片
     */
    imageManager.saveMaterialDict(gl, collada);

    /**
     * 获取图片路径
     */
    let texture = gl.createTexture();
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);
    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, texture);

    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);

    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, gl.RGB, gl.UNSIGNED_BYTE, document.getElementById('testImg'));
    gl.uniform1i(gl.getUniformLocation(gl.program, 'u_Sampler'), 0);
    /*
     * 添加模型
     */
    let library_geometries = collada.library_geometries;
    let geometry = library_geometries[0].geometry;

    for (let i = 0; i < geometry.length; i++) {
        testRenderAllGeom(gl, geometry[i], vertexManager);
    }
    controller.fire('vertices-load');
}

function testRenderAllGeom(gl, geometry, vertexManager) {
    let mesh = geometry.mesh;

    let _source = mesh[0].source;
    let point_source = _source[0];
    let point_float_array = point_source.float_array;
    let point_float_array_data = point_float_array[0]._;
    let normal_source = _source[1];
    let normal_float_array = normal_source.float_array;
    let normal_float_array_data = normal_float_array[0]._;
    let map_source = _source[2];
    let map_float_array = map_source.float_array;
    let map_float_array_data = map_float_array[0]._;

    var vertices = new Float32Array(point_float_array_data.split(' '));
    var texcoords = new Float32Array(map_float_array_data.split(' '));
    var diffTexcoords = [];

    let _triangle = mesh[0].triangles;
    let indicesArrayData = [];
    let materialArrayData = [];
    let shrinkIndices = [];

    for (let i = 0; i < _triangle.length; i++) {
        let triangle_$ = _triangle[i].$;
        let triangle_$_material = triangle_$.material;
        materialArrayData.push(triangle_$_material);
        /**
         * 下面结构体大致内容
         * offset:"0"
         * semantic:"VERTEX" 语义  NORMAL  TEXCOORD
         * source:"#geo
         * @type {*}
         */
        let triangle_p = _triangle[i].p;
        let triangle_p_data = triangle_p[0];

        let indices = new Uint16Array(triangle_p_data.split(' '));

        if (indices[0] != indices[2]) {
            for (let ii = 2; ii < indices.length; ii += 3) {
                /**
                 * 下面是两个索引
                 */
                let key = indices[ii-2];
                let dest = indices[ii];

                diffTexcoords[key*2] = texcoords[dest*2];
                diffTexcoords[key*2+1] = texcoords[dest*2+1];
            }
            texcoords = new Float32Array(diffTexcoords);
        }

        for (let ii = 0; ii < indices.length; ii += 3) {
            shrinkIndices.push(indices[ii]);
        }

        indicesArrayData.push(new Uint16Array(shrinkIndices));
    }


    /**
     * 这里注意方法第二个参数不对。
     */
    var trianglesCount = vertexManager.saveOneCache(gl, geometry.$.id, vertices, indicesArrayData, texcoords, materialArrayData);
    for (let i = 0; i < trianglesCount; i++) {
        let count = vertexManager.bindOneCache(gl, geometry.$.id, i);
        gl.drawElements(gl.TRIANGLES, count, gl.UNSIGNED_BYTE, 0);
    }

}



module.exports = loadDae;