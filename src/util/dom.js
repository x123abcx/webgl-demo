
exports.create = function (tagName, className, container) {
    const el = window.document.createElement(tagName);
    if (className) el.className = className;
    el.width = document.body.scrollWidth;
    el.height = document.body.scrollHeight;
    if (container) container.appendChild(el);

    return el;
};

