/**
 * Created by zhoumingrui on 2017/2/13.
 */
import Vue from 'vue'

Vue.config.keyCodes.a = 65
Vue.config.keyCodes.b = 66
Vue.config.keyCodes.c = 67
Vue.config.keyCodes.d = 68
Vue.config.keyCodes.e = 69
Vue.config.keyCodes.f = 70
Vue.config.keyCodes.g = 71
Vue.config.keyCodes.h = 72
Vue.config.keyCodes.i = 73
Vue.config.keyCodes.j = 74
Vue.config.keyCodes.k = 75
Vue.config.keyCodes.l = 76
Vue.config.keyCodes.m = 77
Vue.config.keyCodes.n = 78
Vue.config.keyCodes.o = 79
Vue.config.keyCodes.p = 80
Vue.config.keyCodes.q = 81
Vue.config.keyCodes.r = 82
Vue.config.keyCodes.s = 83
Vue.config.keyCodes.t = 84
Vue.config.keyCodes.u = 85
Vue.config.keyCodes.v = 86
Vue.config.keyCodes.w = 87
Vue.config.keyCodes.x = 88
Vue.config.keyCodes.y = 89
Vue.config.keyCodes.z = 90