/**
 * Created by zhoumingrui on 2017/1/13.
 */
class ImageManager {
    constructor(imageCache) {
        this.imageCache = imageCache || {};
        this.materialDict = {};
        this.effectDict = {};
    }

    /**
     * 这里就是加载material对应的图片的索引
     * @param collada
     */
    saveMaterialDict(gl, collada) {
        /**
         * 获取图片
         */
        let library_images = collada.library_images;
        this.loadImages(gl, library_images[0].image);
        /**
         * 初始化effectDict
         */
        this.loadEffectConnection2Img(collada);
        /**
         * 获取材质
         */
        let library_materials = collada.library_materials;
        let materials = library_materials[0].material;

        for (let i = 0; i < materials.length; i++) {
            let material = materials[i];
            let id = material.$.id;
            let name = material.$.name;
            let instance_effect = material.instance_effect[0];
            let effect_url = instance_effect.$.url;

            this.materialDict[id] = this.effectDict[effect_url];

        }
    }

    loadEffectConnection2Img(collada){
        /**
         * 获取渲染特效
         */
        let library_effects = collada.library_effects;
        let effects = library_effects[0].effect;
        for(let i=0;i<effects.length;i++){
            let effect = effects[i];
            let effect_id = effect.$.id;
            let img_id = effect.profile_COMMON[0].newparam[0].surface[0].init_from[0];
            this.effectDict['#'+effect_id] = img_id;
        }

    }

    loadImages(gl, imgUrl) {
        /**
         "__TEXTURES_COREAREA_ROAD_WDL0000001_png"
         "./TEXTURES/COREAREA/ROAD/WDL0000001.png"
         */
        for (let i = 0; i < imgUrl.length; i++) {
            let id = imgUrl[i].$.id;
            let url = imgUrl[i].init_from[0];
            this.saveOne(gl, url, id);
        }
    }

    getImageByImgId(id) {
        return this.imageCache[id] || document.getElementById('testImg');
    }

    bindOneImgById(gl,id){
        let imgCache = this.getImageByMaterial(id);
        if(imgCache.texture){
            gl.activeTexture(gl.TEXTURE0);
            gl.bindTexture(gl.TEXTURE_2D, imgCache.texture);
            gl.uniform1i(gl.getUniformLocation(gl.program, 'u_Sampler'), 0);
        }

    }

    /**
     * 通过图片索引来返回数据
     * @param materialId
     * @returns {null}
     */
    getImageByMaterial(materialId) {
        let imageCache = this.imageCache[this.materialDict[materialId]];
        return imageCache || document.getElementById('testImg');
    }

    saveOne(gl, src, id) {
        let image = new Image();
        let that = this;
        image.onload = function(){
            let texture = gl.createTexture();
            gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);
            gl.activeTexture(gl.TEXTURE0);
            gl.bindTexture(gl.TEXTURE_2D, texture);

            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);

            gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, gl.RGB, gl.UNSIGNED_BYTE, image);
            gl.uniform1i(gl.getUniformLocation(gl.program, 'u_Sampler'), 0);

            that.imageCache[id] = {
                image: image,
                texture: texture
            };
        }
        image.src = src;
    }

}

module.exports = ImageManager;