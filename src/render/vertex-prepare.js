/**
 * Created by zhoumingrui on 2017/1/13.
 */
class VertexManager {
    constructor(vertexCache) {
        this.vertexCache = vertexCache || {}
        this.VSTEP = Float32Array.BYTES_PER_ELEMENT;
        this.ISTEP = Uint16Array.BYTES_PER_ELEMENT;
        this.TSTEP = Float32Array.BYTES_PER_ELEMENT;
    }

    /**
     * 目前来说indexData可能为多个，这个需要处理。
     * @param gl
     * @param id
     * @param vertexData
     * @param indexData
     * @param texCoordData
     * @param materialID
     */
    saveOneCache(gl, id, vertexData, indexDataArray, texCoordData, materialIDS) {
        var vertices = vertexData;

        var texcoords = texCoordData;
        /**
         * 第二部创建缓冲区
         * @type {number}
         */

        var vertexBuffer = gl.createBuffer();
        var texcoordBuffer = gl.createBuffer();
        /**
         * 顶点绑定
         */
        gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);
        /**
         * 第三部绑定到对应指针
         */
        var a_pos = gl.getAttribLocation(gl.program, "a_Position");
        gl.vertexAttribPointer(a_pos, 3, gl.FLOAT, false, this.VSTEP * 3, 0);
        gl.enableVertexAttribArray(a_pos);

        /**
         * 纹理绑定
         */
        gl.bindBuffer(gl.ARRAY_BUFFER, texcoordBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, texcoords, gl.STATIC_DRAW);

        /**
         * 第三部绑定到对应指针
         */
        var a_Coord = gl.getAttribLocation(gl.program, "a_TexCoord");
        gl.vertexAttribPointer(a_Coord, 2, gl.FLOAT, false, this.TSTEP * 2, 0);
        gl.enableVertexAttribArray(a_Coord);

        /**
         * 第四步循环绑定索引数据和所有的顶点索引buffer。
         */
        let _indexArrayLength = [];
        let _iBS = [];
        for(let i = 0;i<indexDataArray.length;i++){
            let indices = indexDataArray[i];
            let indiceBuffer = gl.createBuffer();
            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indiceBuffer);
            gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, indices, gl.STATIC_DRAW);
            _indexArrayLength.push(indexDataArray[i].length);
            _iBS.push(indiceBuffer);
        }
        /**
         * IndexArrayLength 是 有若干个index的时候，每个里面的量的统计，IndexCount是有多少个索引数组  materialIDS 是数组。
         * @type {{VB: number, IB: WebGLBuffer, TB: WebGLBuffer, IndexArrayLength: Array, IndexCount, materialIDS: *}}
         */
        this.vertexCache[id] = {
            VB: vertexBuffer,
            IBS: _iBS,
            TB: texcoordBuffer,
            IndexArrayLength:_indexArrayLength,
            IndexCount:indexDataArray.length,
            materialIDS: materialIDS
        }

        return indexDataArray.length;
    }

    /**
     *
     * @param gl
     * @param id
     * @param indexI 画到第几个triangle了。
     * @returns {*}
     */
    bindOneCache(gl, id, indexI) {
        let oneCache = this.vertexCache[id];
        if (oneCache) {
            gl.bindBuffer(gl.ARRAY_BUFFER, oneCache.VB);
            let a_pos = gl.getAttribLocation(gl.program, "a_Position");
            gl.vertexAttribPointer(a_pos, 3, gl.FLOAT, false, this.VSTEP * 3, 0);
            gl.enableVertexAttribArray(a_pos);

            /**
             * 纹理绑定
             */
            gl.bindBuffer(gl.ARRAY_BUFFER, oneCache.TB);
            /**
             * 第三部绑定到对应指针
             */
            var a_Coord = gl.getAttribLocation(gl.program, "a_TexCoord");
            gl.vertexAttribPointer(a_Coord, 2, gl.FLOAT, false, this.TSTEP * 2, 0);
            gl.enableVertexAttribArray(a_Coord);
            /**
             * 第四步绑定索引数据
             */
            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, oneCache.IBS[indexI]);

            return oneCache.IndexArrayLength[indexI];
        }
        return 0;
    }

    getOneCache(id) {
        return this.vertexCache[id];
    }
}

module.exports = VertexManager;