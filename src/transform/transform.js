/**
 * Created by zhoumingrui on 2017/2/13.
 */
import glMatrix from 'gl-matrix'
const mat4 = glMatrix.mat4;

var zoom = 0;
var _angle = 0, _x = 0, _y = 0, _z = 0;
var _fov = 0.6435011087932844;
var _pitch = 0;


class Transform {
    static getInstance() {
        if (!this.instance) {
            this.instance = new Transform();
        }
        return this.instance;
    }

    resize(width, height) {
        this.width = width;
        this.height = height;
    }

    get height() {
        return this._height;
    }

    set height(h) {
        this._height = h;
    }

    get width() {
        return this._width;
    }

    set width(w) {
        this._width = w;
    }

    calMatrix(angle, x, y, z) {
        _x = x ? x : _x;
        _y = y ? y : _y;
        _z = z ? z : _z;
        _angle = angle ? angle : _angle;

        let _posMatrix = mat4.identity(new Float32Array(16));
        let cameraToCenterDistance = 0.5 / Math.tan(_fov / 2) * this.height;
        /*
         * 投影视图矩阵不是mvp么，那么应该是model view projection
         */
        /**
         * 投影矩阵 projectionMatrix
         */
        mat4.perspective(_posMatrix, _fov, this.width / this.height, 1, cameraToCenterDistance);
        /**
         * 这个modelMatrix。
         */
        let modelMatrix = mat4.identity(new Float32Array(16));
        mat4.rotateX(_posMatrix, _posMatrix, _angle);
        mat4.translate(_posMatrix, _posMatrix, [-1010.947510 + _x, -170.040001 + _y, -1030.358582 + _z])

        //mat4.multiply(_posMatrix, _posMatrix, modelMatrix);

        return _posMatrix;
    }
}

module.exports = Transform;