/**
 * Created by zhoumingrui on 2017/1/7.
 */
const textureVP = require('./texture.vp.glsl')
const textureFP = require('./texture.vp.glsl')
const lineVP = require('./line.vp.glsl')
const lineFP = require('./line.fp.glsl')

module.exports = {
    texture: {vp: textureVP, fp: textureFP},
    line:{vp: lineVP, fp: lineFP}
}