/**
 * Created by zhoumingrui on 2017/2/4.
 */
'use strict';

var algorithm = require('../../src/util/navi-mesh-algorithm');
var expect = require('chai').expect;

const al = algorithm.getInstance();

describe('测试依据图幅号计算zoom level', function () {
    it('2477029765的level应该等于 9', function () {
        expect(al.calLevelByMesh(2477029765)).to.be.equal(9);
    });

    it('2477029765的RectPos是', function () {
        expect(al.calRectPosByMesh(2477029765, 9)).to.be.deep.equal({
                "iLBX": 1768423424,
                "iLBY": 482344960,
                "iRTX": 1518338048,
                "iRTY": 220200960
            }
        );
    });

    it('2477029765的经度是116.38', function () {
        expect(al.convertPosToLon(1263271936)).to.be.equal(116.38);
    });

    it('2477029765的纬度是39.38', function () {
        expect(al.convertPosToLat(241172480)).to.be.equal(39.38);
    });

});